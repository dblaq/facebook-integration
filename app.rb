#####################################################
# CONFIG
# {{{
require 'sinatra'
require 'gibbon'
require 'logger'
require 'erubis'

configure :development do
  # Set up logging
  Dir.mkdir('logs') unless File.exist?('logs')
  $log = Logger.new('logs/output.log','weekly')
  $log.level = Logger::DEBUG
end

# Globally set erubis to render with auto-escaping of html
set :erubis, :escape_html => true

#####################################################
# ROUTES

# /
# {{{
get '/' do
  erubis :index
end
#}}}

#####################################################
# ERROR STUFF

# /error
# {{{
get '/error' do
  erubis :error
end
#}}}
# vim:fdm=marker:ff=unix:nowrap
